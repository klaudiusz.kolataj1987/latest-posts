const express = require("express");
const mongodb = require("mongodb");

const router = express.Router();

// Get Posts
router.get("/", async (req, res) => {
  const posts = await loadPostsCollection();
  res.send(await posts.find({}).toArray());
});

// Add Posts
router.post("/", async (req, res) => {
  const posts = await loadPostsCollection();
  await posts.insertOne({
    content: req.body.formData,
    user: req.body.user,
    createdAt: new Date()
  });
  res.status(201).send();
});

// Delete Posts
router.delete("/:id", async (req, res) => {
  const posts = await loadPostsCollection();
  await posts.deleteOne({ _id: new mongodb.ObjectID(req.params.id) });
  res.status(200).send();
});

// Like Post
router.post("/:id", async (req, res) => {
  const posts = await loadPostsCollection();
  const userId = req.body.userId;
  await posts
    .findOne({ _id: new mongodb.ObjectID(req.params.id) })
    .then(postItem => {
      if (
        postItem &&
        postItem.content.likes.find(item => item.id === userId.id)
      ) {
        posts.updateOne(
          { _id: new mongodb.ObjectID(req.params.id) },
          { $pull: { "content.likes": userId } }
        );
      } else {
        posts.updateOne(
          { _id: new mongodb.ObjectID(req.params.id) },
          { $push: { "content.likes": userId } }
        );
      }
    });

  res.status(200).send();
});

async function loadPostsCollection() {
  const client = await mongodb.MongoClient.connect(
    "mongodb+srv://cloud_admin:fDqeHCX75NyF9DPk@cluster0-jvi6z.mongodb.net/test?retryWrites=true&w=majority",
    {
      // useNewUrlParser: true,
      useUnifiedTopology: true
    }
  );
  return client.db("test").collection("posts");
}

module.exports = router;
