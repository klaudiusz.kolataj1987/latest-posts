const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const path = require("path");
const cors = require("cors");
const passport = require("passport");

const app = express();

// Middlewares

// Form Data Middleware
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
// Json Body Middleware
app.use(bodyParser({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(bodyParser.json());
// Cors Middleware
app.use(cors());

const posts = require("./routes/api/posts");

app.use("/api/posts", posts);

// Use passport Middleware
app.use(passport.initialize());
// Bring in the Passport Strategy
require("./config/passport")(passport);

const users = require("./routes/api/users");
app.use("/api/users", users);

// Handle production
if (process.env.NODE_ENV === "production") {
  // Static folder
  app.use(express.static(__dirname + "/public/"));

  // Handle SPA
  app.get("*", (req, res) => res.sendFile(__dirname + "public/index.html"));
}

const port = process.env.PORT || 5000;

// Bring in the Database Config and connect with database
const db = require("./config/keys").mongoURI;
mongoose
  .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log(`Server listening on port: ${db}`);
  })
  .catch(err => {
    console.log("Unable to connect with database", err);
  });

app.listen(port, () => console.log(`Server listening on port: ${port}`));
